Given(/^these rents exist:$/) do
    rent1 = Rent.create!("cost" => 30, "location" => "Tallinn", "area" => 60, "floor" => 3, "services" => 'kitchen')
    booking1 = Booking.create!("start" => "25-Nov-2017", "end" => "29-Nov-2017", "rent_id" => rent1.id)
end

# Given /^the following accounts:$/ do |class_name, table|
#   table.hashes.each do |attributes|
#     u = User.make! :name => attributes[:user]
#     Account.make! :user => u
#   end
# end
#
# Given(/^the following loans exist:$/) do |table|
#     book = Book.create!("type" => "B", "callno" => 2, "title" => "The Ruby way", "authors" => "Some authors")
#     user = User.create!("username" => "Cucu")
#     loan = Loan.create!("start_date" => "2016-01-01", "return_date" => "2016-01-01", "cost" => 2.0, "book_id" => book.id, "user_id" => user.id)
# end
