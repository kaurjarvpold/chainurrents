Feature: Searching for Parking Rents

  As a potential customer,
  I want to search for rents in a certain place and time,
  so that I can have a place to stay.

Background: Rents have been added to database

  Given these rents exist:

Scenario: 'Location has rents'

  Given I am on the front page
  And I fill in "location" with "Tallinn"
  And I fill in "start_date" with "25-Nov-2017"
  And I fill in "end_date" with "28-Nov-2017"
  And I press "Submit"
  Then I should see "35"
  And should not see "30"
  And I should not see "Tartu"

Scenario: 'Location does not have rents'

  Given I am on the front page
  And I fill in "location" with "Kuressaare"
  And I fill in "start_date" with "25-Nov-2017"
  And I fill in "end_date" with "28-Nov-2017"
  And I press "Submit"
  Then I should see "Sorry, there are no rents available at this location."

Scenario: 'Invalid date'

  Given I am on the front page
  And I fill in "location" with "Tallinn"
  And I fill in "start_date" with "blabla"
  And I fill in "end_date" with "blabla"
  And I press "Submit"
  Then I should see "Please enter a valid date."
