
rent1 = Rent.create!(:cost => 30, :location => 'Tallinn', :area => 30, :floor => 2, :services => 'kitchen')
rent2 = Rent.create!(:cost => 40, :location => 'Tartu', :area => 50, :floor => 1, :services => 'garage')


booking1 = Booking.create!(:startDate => '25-Nov-2017', :endDate => '29-Nov-2017', :rent => rent1)
booking2 = Booking.create!(:startDate => '25-Nov-2017', :endDate => '29-Nov-2017', :rent => rent2)
