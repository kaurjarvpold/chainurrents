# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20170110111239) do

  # These are extensions that must be enabled in order to support this database
  enable_extension "plpgsql"

  create_table "bookings", force: :cascade do |t|
    t.integer  "rent_id"
    t.datetime "startDate"
    t.datetime "endDate"
    t.index ["rent_id"], name: "index_bookings_on_rent_id", using: :btree
  end

  create_table "rents", force: :cascade do |t|
    t.integer  "cost"
    t.string   "location"
    t.integer  "area"
    t.string   "floor"
    t.text     "services"
    t.datetime "findStart"
    t.datetime "findEnd"
  end

end
