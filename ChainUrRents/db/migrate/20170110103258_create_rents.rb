class CreateRents < ActiveRecord::Migration[5.0]
  def change
    create_table :rents do |t|
      t.integer :cost
      t.string :location
      t.integer :area
      t.string :floor
      t.text :services
    end

    create_table :bookings do |t|
      t.belongs_to :rent, index: true
      t.datetime :startDate
      t.datetime :endDate
    end
  end
end
