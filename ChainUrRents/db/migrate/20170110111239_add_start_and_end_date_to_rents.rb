class AddStartAndEndDateToRents < ActiveRecord::Migration[5.0]
  def change
    add_column :rents, :findStart, :datetime
    add_column :rents, :findEnd, :datetime
  end
end
