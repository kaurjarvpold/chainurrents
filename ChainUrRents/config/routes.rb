Rails.application.routes.draw do
  get "/rents" => "rents#index"
  post "/show" => "rents#show"
  get "/show" => "rents#show"
  root to: redirect('/rents')
end
