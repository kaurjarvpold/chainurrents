require 'spec_helper'
require 'rails_helper'


describe Rent, type: :model do
  subject { described_class.new }

  it "is valid with valid attributes" do
    subject.cost = 30
    subject.location = "Tallinn"
    subject.area = 50
    subject.floor = 3
    expect(subject).to be_valid
  end
  it "is not valid without a cost" do
    expect(subject).to_not be_valid
  end
  it "is not valid without a location" do
    subject.cost = 30
    expect(subject).to_not be_valid
  end
  it "is not valid without a area" do
    subject.cost = 30
    subject.location = "Tallinn"
    expect(subject).to_not be_valid
  end
  it "is not valid without the floor" do
    subject.cost = 30
    subject.location = "Tallinn"
    subject.area = 50
    expect(subject).to_not be_valid
  end
end
