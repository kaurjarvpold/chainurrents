require 'spec_helper'
require 'rails_helper'


describe Booking, type: :model do
  subject { described_class.new }

  it "is valid with valid attributes" do
    subject.start = '25-Nov-2017'
    subject.end = '29-Nov-2017'
    subject.rent_id = 1
    expect(subject).to be_valid
  end
  it "is not valid without a start date" do
    expect(subject).to_not be_valid
  end
  it "is not valid without an end date" do
    subject.start = '25-Nov-2017'
    expect(subject).to_not be_valid
  end
  it "is not valid without a rent_id" do
    subject.start = '25-Nov-2017'
    subject.end = '29-Nov-2017'
    expect(subject).to_not be_valid
  end
end
