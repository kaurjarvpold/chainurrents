require 'spec_helper'
require 'rails_helper'


describe LotsController, type: :controller do

  before(:each) do
    @rent    = Rent.create!("cost" => 30, "location" => "Tallinn", "area" => 60, "floor" => 3, "services" => 'kitchen')
    @booking = Booking.create!("start" => "25-Nov-2017", "end" => "29-Nov-2017", "rent_id" => rent1.id)
  end

  describe 'GET index' do
    it 'renders the index view' do
      get :index
      expect(response).to render_template(:index)
    end
    it 'returns HTTP OK status' do
      get :index
      expect(response).to have_http_status(:ok)
    end
  end

  describe 'Post show' do
    it 'renders the show view when correct data is entered' do
      post :show, rents: {location: 'Tallinn', available_start: '10-Nov-2017', available_end: '15-Nov-2017'}
      expect(response).to render_template(:show)
    end
    it 'displays free rents' do
      post :show, lots: {location: 'Tallinn', available_start: '10-Nov-2017', available_end: '15-Nov-2017'}
      expect(response).to have_http_status(:success)
    end
  end

end
